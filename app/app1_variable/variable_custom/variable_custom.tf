variable "roro" {
  type = string
  default = "lapin"
}

variable "lala" {
  type = string
  default = "mignon"
}



resource "null_resource" "node1" {
  count = 2
  triggers = {
    lala = "mignon"
  }
  provisioner "local-exec" {
    command = "echo '${var.lala}'"
  }
}

resource "null_resource" "nunu" {
  triggers = {
    lala = "mignon"
  }
  count       = length(null_resource.node1)
  provisioner "local-exec" {
    command = "echo 'Ceci est un exemple de ${count.index}'"
  }
}



# output "sortie" {
#   value = null_resource.nunu
# }