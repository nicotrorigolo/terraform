variable "lapin" {
  type = string
  default = "127.0.0.1 gitlab-test"
}

resource "null_resource" "node1" {
  provisioner "local-exec" {
    command = "echo '${var.lapin}' > catapulte.txt"
  }
}

output "lapin" {
  value = var.lapin
}