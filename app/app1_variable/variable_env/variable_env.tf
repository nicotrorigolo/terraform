variable "roro" {
  type = string
}

resource "null_resource" "node1" {
  provisioner "local-exec" {
    command = "echo '${var.roro}' > catapulte.txt"
  }
}

output "roro" {
  value = var.roro
}