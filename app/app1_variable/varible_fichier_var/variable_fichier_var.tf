variable "lapin" {
  type = string
}

resource "null_resource" "node1" {
  provisioner "local-exec" {
    command = "echo '${var.lapin}' > catapulte.txt"
  }
}

output "lapin" {
  value = var.lapin
}