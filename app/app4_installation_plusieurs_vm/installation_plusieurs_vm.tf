variable "instance1" {
  type = map
  default = {
    name = "instance1"
    ip = "20.0.0.10"
  }
}

variable "instance2" {
  type = map
  default = {
    name = "instance2"
    ip = "20.0.0.11"
  }
}

# Le provider est récuperer sur le réseau
terraform {
  required_providers {
    libvirt = {
      source = "dmacvicar/libvirt"
      version = "0.7.1"
    }
  }
}

provider "libvirt" {
  uri = "qemu:///system"
}

# Création d'un network
resource "libvirt_network" "terraform_vm_network" {
  name = "terraform_vm_network"
  addresses = [
    "20.0.0.0/20"
    ]
  mode = "nat"
  dhcp {
    enabled = true
  }
}

# Création d'une pool
resource "libvirt_pool" "tabouret_pool" {
  name = "tabouret_pool"
  type = "dir"
  path = "/var/lib/libvirt/images_tabouret_pool"
}

module "instance1" {
  source = "./modules/instances"
  name = var.instance1.name
  network_id = libvirt_network.terraform_vm_network.id
  pool = libvirt_pool.tabouret_pool.name
  ip = var.instance1.ip
}

module "instance2" {
  source = "./modules/instances"
  name = var.instance2.name
  network_id = libvirt_network.terraform_vm_network.id
  pool = libvirt_pool.tabouret_pool.name
  ip = var.instance2.ip
}

# # Afficher l'adresse im de la vm
# output "ip" {
#   value = libvirt_network.terraform_vm_network
# }
