# Declaration d'un fichier user_data a executer
data "template_file" "user_data" {
  template = file("${path.module}/cloud_init.cfg")
}

# Lecture du fichier a executer
resource "libvirt_cloudinit_disk" "commoninit" {
  name = "commoninit_${var.name}.iso"
  pool = "${var.pool}"
  user_data = data.template_file.user_data.rendered
}
