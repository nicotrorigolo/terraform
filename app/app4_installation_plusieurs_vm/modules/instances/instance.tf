# On indique les caractéristique de la vm
resource "libvirt_domain" "ubuntu_vm" {
  name = "ubuntu_${var.name}"
  memory = 3072
  vcpu   = 2
  cloudinit = libvirt_cloudinit_disk.commoninit.id          # Execution du fichier
  network_interface {
    network_id = "${var.network_id}"
    addresses = [
        "${var.ip}"
        ]
  }
  disk {
    volume_id = libvirt_volume.test-os_image.id
  }
  console {
    type = "pty"
    target_type = "serial"
    target_port = "0"
  }
  graphics {
    type = "spice"
    listen_type = "address"
    autoport = true
  }
}
