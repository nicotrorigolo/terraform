variable "instance" {
  type = map
  default = {
    name = "instance"
    nombre_instance = 3
    ip = "20.0.0.1"
  }
}

# Le provider est récuperer sur le réseau
terraform {
  required_providers {
    libvirt = {
      source = "dmacvicar/libvirt"
      version = "0.7.1"
    }
  }
}

provider "libvirt" {
  uri = "qemu:///system"
}

# Création d'un network
resource "libvirt_network" "terraform_vm_network" {
  name = "terraform_vm_network"
  addresses = [
    "20.0.0.0/20"
    ]
  mode = "nat"
  dhcp {
    enabled = true
  }
}

# Création d'une pool
resource "libvirt_pool" "tabouret_pool" {
  name = "tabouret_pool"
  type = "dir"
  path = "/var/lib/libvirt/images_tabouret_pool"
}

module "instance" {
  source = "gitlab.com/nico-terraform/instance/local"
  version = "1.0.0"
  nombre_instance = var.instance.nombre_instance
  name = var.instance.name
  network_id = libvirt_network.terraform_vm_network.id
  pool = libvirt_pool.tabouret_pool.name
  ip = var.instance.ip
}

resource "null_resource" "inventory_init" {
  provisioner "local-exec" {
    command = "ansible-playbook -i \"localhost,\" -c local --tags wait initialisation_provisioning.yaml"
  }
  depends_on = [
    module.instance
  ]
}

resource "null_resource" "provisionning_init" {
  provisioner "local-exec" {
    command = "ansible-playbook -i inventory.py --skip-tags wait initialisation_provisioning.yaml"
  }
  depends_on = [
    null_resource.inventory_init
  ]
}
