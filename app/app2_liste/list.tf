variable "lapin" {
  default = [
    "127.0.0.1",
    "tabouret",
    "voiture"
  ]
}

resource "null_resource" "lapin" {
  count = "${length(var.lapin)}"
  triggers = {
    chat = element(var.lapin, count.index)
  }
  provisioner "local-exec" {
    command = "echo '${element(var.lapin, count.index)}' >> catapulte.txt"
  }
}

output "lapin" {
  value = var.lapin
}
