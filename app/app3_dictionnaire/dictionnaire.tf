variable "lapin" {
  default = {
    "127.0.0.1" = "gitlab-test1542"
    "tabouret" = "maison-test"
    "voiture" = "vroum_vroum"
    }
}

resource "null_resource" "lapin" {
  for_each = var.lapin
  triggers = {
    chat = each.value
  }
  provisioner "local-exec" {
    command = "echo '${each.key} ${each.value}' >> catapulte.txt"
  }
}

output "lapin" {
  value = var.lapin
}
