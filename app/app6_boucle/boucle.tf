variable "lapin" {
  type = map
  default = {
    debut = 0
    fin = 20
    }
}

locals {
  boucle = {
    for i in range(var.lapin.debut, var.lapin.fin + 1):
      "koko${i}" => {
        "bunny" = i
        "hoho${i}" = "catapulte"
    }
  }
}

output "sortie" {
  value = local.boucle
}
