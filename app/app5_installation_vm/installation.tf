# Le provider est récuperer sur le réseau
terraform {
  required_providers {
    libvirt = {
      source = "dmacvicar/libvirt"
      version = "0.7.1"
    }
  }
}

provider "libvirt" {
  uri = "qemu:///system"
}

# Création d'un volume pour stocker l'image de l'OS
resource "libvirt_volume" "ubuntu_qcow2" {
  name   = "ubuntu_qcow2"
  pool   = "default"
  source = "https://cloud-images.ubuntu.com/releases/focal/release/ubuntu-20.04-server-cloudimg-amd64.img"
  format = "qcow2"
}

# Création d'un network
resource "libvirt_network" "terraform_vm_network" {
  name = "terraform_vm_network"
  addresses = [
    "20.0.0.0/20"
    ]
  mode = "nat"
  dhcp {
    enabled = true
  }
}

# Declaration d'un fichier user_data a executer
data "template_file" "user_data" {
  template = file("${path.module}/cloud_init.cfg")
}

# Lecture du fichier a executer
resource "libvirt_cloudinit_disk" "commoninit" {
  name = "commoninit.iso"
  user_data = data.template_file.user_data.rendered
}

# On indique les caractéristique de la vm
resource "libvirt_domain" "ubuntu_vm" {
  name = "ubuntu"
  memory = "1024"
  vcpu   = 1
  cloudinit = libvirt_cloudinit_disk.commoninit.id          # Execution du fichier
  network_interface {
    network_id = libvirt_network.terraform_vm_network.id
    addresses    = ["20.0.0.10"]
  }
  disk {
    volume_id = libvirt_volume.ubuntu_qcow2.id
  }
  console {
    type = "pty"
    target_type = "serial"
    target_port = "0"
  }
  graphics {
    type = "spice"
    listen_type = "address"
    autoport = true
  }
}

# # Afficher l'adresse im de la vm
# output "ip" {
#   value = libvirt_network.terraform_vm_network
# }
